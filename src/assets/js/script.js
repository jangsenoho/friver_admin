

$(document).ready(function () {

  // list sort 
  jQuery(function ($) {
    var panelList = $('#draggablePanelList');

    panelList.sortable({
      // Only make the .panel-heading child elements support dragging.
      // Omit this to make then entire <li>...</li> draggable.
      handle: '.panel-heading',
      update: function () {
        $('.panel', panelList).each(function (index, elem) {
          var $listItem = $(elem),
            newIndex = $listItem.index();

          // Persist the new indices.
        });
      }
    });
  });

  jQuery(function ($) {
    var panelList2 = $('#draggablePanelList2');

    panelList2.sortable({
      // Only make the .panel-heading child elements support dragging.
      // Omit this to make then entire <li>...</li> draggable.
      handle: '.panel-heading',
      update: function () {
        $('.panel', panelList2).each(function (index, elem) {
          var $listItem = $(elem),
            newIndex = $listItem.index();

          // Persist the new indices.
        });
      }
    });
  });
  //drg and drop table 
  $(function () {
    $("#sortable tbody").sortable({
      cursor: "move",
      placeholder: "sortable-placeholder",
      helper: function (e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function (index) {
          // Set helper cell sizes to match the original sizes
          $(this).width($originals.eq(index).width());
        });
        return $helper;
      }
    }).disableSelection();
  });

  //datepicker
  $("input[id^='datepicker']").each(function () {
    var _this = this.id;
    $('#' + _this).datepicker({
      regional: 'ko',
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
      showOn: "button",
      buttonImage: "../assets/images/ico-calendar.png",
      buttonImageOnly: true,
    });
  });
  //팝업
$('.user-info-pop').magnificPopup({
  type: 'iframe',
  closeOnContentClick: true,
  closeBtnInside: false,
  mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
});
})
